input = `5 127 680267 39260 0 26 3553 5851995`
// input = `125 17`

stones = input.split(' ').map(s => Number(s))

for (let i = 0; i < 25; i++) {
	console.log(stones)
	for (let j = 0; j < stones.length; j++) {
		stone = stones[j]
		if (stone == 0) {
			stones[j] = 1
			continue
		}
		if (Math.floor(Math.log10(stone)) %2 == 1) {
			newStone1 = Math.floor(stone/Math.pow(10, Math.ceil(Math.log10(stone)/2)))
			newStone2 = stone % Math.pow(10, Math.ceil(Math.log10(stone)/2))
			stones.splice(j, 1, newStone1, newStone2)
			j++
			continue
		}
		stones[j] = stone * 2024
	}
}
console.log(stones.length)
