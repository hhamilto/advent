fs = require('fs')
input = fs.readFileSync('day3-input.txt').toString()

regexp = new RegExp('(mul\\(\\d+,\\d+\\)|do\\(\\)|don\'t\\(\\))', 'g')
const array = [...input.matchAll(regexp)].map(m => {
	instruction = m.shift()
	if (instruction == "don't()") {
		instruction = 'dont()'
	}
	if (instruction == "do()") {
		instruction = 'doo()'
	}
	return instruction;
})
console.log(array)
res = eval('is_doing = true; acc = 0; function mul(x,y) { if(is_doing) acc+=x*y}; function doo() {is_doing = true}; function dont(){is_doing = false};'+array.join(';') + ';acc')
console.log(res)
