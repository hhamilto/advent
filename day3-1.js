fs = require('fs')
input = fs.readFileSync('day3-input.txt').toString()

regexp = new RegExp('mul\\(\\d+,\\d+\\)', 'g')
const array = [...input.matchAll(regexp)].map(m => m.shift());
res = eval('function mul(x,y) { return x*y}'+array.join('+'))
console.log(res)
