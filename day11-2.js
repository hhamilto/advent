input = `5 127 680267 39260 0 26 3553 5851995`
// input = `125 17`

stones = input.split(' ').map(s => Number(s))

cache = {}

stoneCount = (stone, iterations) => {
	if (iterations == 0) {
		return 1
	}
	if (cache[stone +'.'+iterations]) {
		return cache[stone +'.'+iterations]
	}
	if (stone == 0) {
		let count = stoneCount(1, iterations-1)
		cache[stone +'.'+iterations] = count
		return count
	}
	if (Math.floor(Math.log10(stone)) %2 == 1) {
		let newStone1 = Math.floor(stone/Math.pow(10, Math.ceil(Math.log10(stone)/2)))
		let newStone2 = stone % Math.pow(10, Math.ceil(Math.log10(stone)/2))
		let count1 = stoneCount(newStone1, iterations -1)
		let count2 = stoneCount(newStone2, iterations -1)
		let count = count1+count2
		cache[stone +'.'+iterations] = count
		return count
	}
	let count = stoneCount(stone * 2024, iterations -1)
	cache[stone +'.'+iterations] = count
	return count
}

acc = 0;
ITERATIONS=75
for(let i = 0; i < stones.length; i++) {
	acc += stoneCount(stones[i], ITERATIONS)
}
console.log(cache)
console.log(acc)
