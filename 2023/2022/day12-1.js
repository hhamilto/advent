let input = `KF-sr
OO-vy
start-FP
FP-end
vy-mi
vy-KF
vy-na
start-sr
FP-lh
sr-FP
na-FP
end-KF
na-mi
lh-KF
end-lh
na-start
wp-KF
mi-KF
vy-sr
vy-lh
sr-mi`



let caves = {}

let connections = input.split('\n')

for (let i = 0; i < connections.length; i++) {
	const [a,b] = connections[i].split('-')
	if (!caves[a]) {
		caves[a] = {
			connections:[]
		}
	}
	if (!caves[b]) {
		caves[b] = {
			connections:[]
		}
	}
	caves[a].connections.push(b)
	caves[b].connections.push(a)
}

const isLowerCase = (str) => {
	console.log(str)
	return str.toLowerCase() === str
}

const countPaths = (startCaveName, visited = {}) => {
	if (visited[startCaveName]) {
		return 0
	}
	if (startCaveName === 'end') {
		return 1
	}
	if (isLowerCase(startCaveName)) {
		visited[startCaveName] = true
	}
	let pathCount = 0;
	let cave = caves[startCaveName]
	for (let i = 0; i < cave.connections.length; i++) {
		pathCount += countPaths(cave.connections[i], {...visited})
	}
	return pathCount
}

console.log(countPaths('start'))
