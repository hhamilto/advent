let input = `PBVHVOCOCFFNBCNCCBHK

FV -> C
SS -> B
SC -> B
BP -> K
VP -> S
HK -> K
FS -> F
CC -> V
VB -> P
OP -> B
FO -> N
FH -> O
VK -> N
PV -> S
HV -> O
PF -> F
HH -> F
NK -> S
NC -> S
FC -> H
FK -> K
OO -> N
HP -> C
NN -> H
BB -> H
CN -> P
PS -> N
VF -> S
CB -> B
OH -> S
CF -> C
OK -> P
CV -> V
CS -> H
KN -> B
OV -> S
HB -> C
OS -> V
PC -> B
CK -> S
PP -> K
SN -> O
VV -> C
NS -> F
PN -> K
HS -> P
VO -> B
VC -> B
NV -> P
VS -> N
FP -> F
HO -> S
KS -> O
BN -> F
VN -> P
OC -> K
SF -> P
PO -> P
SB -> O
FN -> F
OF -> F
CP -> C
HC -> O
PH -> O
BC -> O
NO -> C
BH -> C
VH -> S
KK -> O
SV -> K
KB -> K
BS -> S
HF -> B
NH -> S
PB -> N
HN -> K
SK -> B
FB -> F
KV -> S
BF -> S
ON -> S
BV -> P
KC -> S
NB -> S
NP -> B
BK -> K
NF -> C
BO -> K
KF -> B
KH -> N
SP -> O
CO -> S
KO -> V
SO -> B
CH -> C
KP -> C
FF -> K
PK -> F
OB -> H
SH -> C`

// input = `NNCB

// CH -> B
// HH -> N
// CB -> H
// NH -> C
// HB -> C
// HC -> B
// HN -> C
// NN -> C
// BH -> H
// NC -> B
// NB -> B
// BN -> B
// BB -> N
// BC -> B
// CC -> N
// CN -> C`

let [polymerString, insertionString] = input.split('\n\n')
// polymerString = 'NBBBCNCCNBBNBNBBCHBHHBCHB'
// polymerString = 'NBCCNBBBCBHCB'
const ROUNDS = 40

const pairs = {}

const pairsToString = (pairs) => {
	return Object.keys(pairs).sort().filter(k => pairs[k]).map(k => pairs[k]+' '+k).join(', ')
}


for (let i = 0; i < polymerString.length-1; i++) {
	const pair = polymerString.substr(i, 2)
	if (!pairs[pair]) {
		pairs[pair] = 0
	}
	pairs[pair]++
}

// pairs['X'+polymerString[0]] = 1
// pairs['X'+polymerString[polymerString.length-1]] = 1
// console.log(pairs)


const insertionRules = insertionString.split('\n').map(s => {
	let [match, replacement] = s.split(' -> ')
	return {
		match: (string) => {
			const matches = []
			for (let i = 0; i < string.length; i++) {
				if (string[i] == match[0] && string[i+1] == match[1]) {
					matches.push(i)
				}
			}
			return matches
		},
		matchString: match,
		replacement
	}
})

// console.log(pairsToString(pairs))

for (let rounds = 0; rounds < ROUNDS; rounds++) {
	const pairsToAdd = {}
	for (const rule of insertionRules) {
		if (pairs[rule.matchString]) {
			newPairs = [rule.matchString[0]+rule.replacement, rule.replacement + rule.matchString[1]]
			if (newPairs.filter(pair => pair === 'BB').length > 0) {
				// console.log(rule.matchString)
				// console.log(pairs[rule.matchString])
				// console.log(newPairs)
			}
			for (const newPair of newPairs) {
				if (!pairsToAdd[newPair]) {
					pairsToAdd[newPair] = 0
				}
				pairsToAdd[newPair] += pairs[rule.matchString]
			}

			if (!pairsToAdd[rule.matchString]) {
				pairsToAdd[rule.matchString] = 0
			}
			pairsToAdd[rule.matchString] -= pairs[rule.matchString]
		}
	}
	for (const newPair of Object.keys(pairsToAdd)) {
		if (!pairs[newPair]) {
			pairs[newPair] = 0
		}
		// console.log(pairsToAdd[newPair])
		pairs[newPair] += pairsToAdd[newPair]
	}
}

console.log(pairsToString(pairs))
// process.exit()

const letterCounts = {}

console.log("START")

for (const pair of Object.keys(pairs)) {
	for (const letter of pair) {
		if (!letterCounts[letter]) {
			letterCounts[letter] = 0
		}
		letterCounts[letter] += pairs[pair]/2
	}
}

console.log(letterCounts)
console.log("END")
let countsSorted = Object.values(letterCounts).sort((a,b) => b-a)

console.log(Math.ceil(countsSorted[0] - countsSorted[countsSorted.length - 1]))


process.exit();


// NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB
// NBBBCNCCNBBNBNBBCHBHHBCHB


console.log(polymerString)
for (let rounds = 0; rounds < 40; rounds++) {
	const insertions = []
	for (let i = 0; i < insertionRules.length; i++) {
		const rule = insertionRules[i]
		const matches = rule.match(polymerString);
		// console.log(matches)
		for (const match of matches) {
			insertions.push({
				index: match+1,
				toInsert: rule.replacement,
				fromRule: rule.matchString
			})
		}
	}
	insertions.sort((a,b) => {
		return b.index - a.index
	})
	// console.log(insertions)
	for (const insertion of insertions) {
		// console.log('before: ', polymerString)
		polymerString = polymerString.substr(0, insertion.index) + insertion.toInsert + polymerString.substr(insertion.index)
		// console.log('after:  ', polymerString)
	}
}

// console.log(polymerString)
