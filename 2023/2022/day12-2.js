let input = `KF-sr
OO-vy
start-FP
FP-end
vy-mi
vy-KF
vy-na
start-sr
FP-lh
sr-FP
na-FP
end-KF
na-mi
lh-KF
end-lh
na-start
wp-KF
mi-KF
vy-sr
vy-lh
sr-mi`

// input = `start-A
// start-b
// A-c
// A-b
// b-d
// A-end
// b-end`


let caves = {}

let connections = input.split('\n')

for (let i = 0; i < connections.length; i++) {
	const [a,b] = connections[i].split('-')
	if (!caves[a]) {
		caves[a] = {
			connections:[]
		}
	}
	if (!caves[b]) {
		caves[b] = {
			connections:[]
		}
	}
	caves[a].connections.push(b)
	caves[b].connections.push(a)
	caves[a].connections.sort()
	caves[b].connections.sort()
}

const isLowerCase = (str) => {
	return str.toLowerCase() === str
}

const countPaths = (startCaveName, visited = {}, littleSpecialCave, option, path) => {
	if (visited[startCaveName]) {
		if (littleSpecialCave || startCaveName == 'start') {
			return 0
		}
		littleSpecialCave = startCaveName
	}
	if (startCaveName === 'end') {
		console.log(path)
		return 1
	}
	if (startCaveName === 'start') {
		visited[startCaveName] = true
	}
	if (isLowerCase(startCaveName)) {
		visited[startCaveName] = true
	}

	// console.log(startCaveName, option)
	let pathCount = 0;
	let cave = caves[startCaveName]
	for (let i = 0; i < cave.connections.length; i++) {
		pathCount += countPaths(cave.connections[i], {...visited}, littleSpecialCave, 3, path + ',' + cave.connections[i])
	}
	return pathCount
}

console.log(countPaths('start', {}, undefined, -1, 'start'))




// const countPaths = (startCaveName, visited = {}, littleSpecialCave, option, path) => {
// 	if (visited[startCaveName]) {
// 		return 0
// 	}
// 	if (startCaveName === 'end') {
// 		console.log(path)
// 		return 1
// 	}
// 	if (startCaveName === 'start') {
// 		visited[startCaveName] = true
// 	}
// 	// console.log(startCaveName, option)
// 	let pathCount = 0;
// 	let cave = caves[startCaveName]
// 	for (let i = 0; i < cave.connections.length; i++) {
// 		if (isLowerCase(startCaveName)) {
// 			if (littleSpecialCave) {
// 				visited[startCaveName] = true
// 				pathCount += countPaths(cave.connections[i], {...visited}, littleSpecialCave, 0, path + ',' + cave.connections[i])
// 			} else {
// 				// Option 2: We let ourselves do it again
// 				pathCount += countPaths(cave.connections[i], {...visited}, startCaveName, 2, path + ',' + cave.connections[i])
// 				// Option 1: We commit to not visiting this cave again
// 				tmpVisited = {...visited}
// 				tmpVisited[startCaveName] = true
// 				pathCount += countPaths(cave.connections[i], tmpVisited, undefined, 1, path + ',' + cave.connections[i])
// 			}
// 		} else {
// 			pathCount += countPaths(cave.connections[i], {...visited}, littleSpecialCave, 3, path + ',' + cave.connections[i])
// 		}
// 	}
// 	return pathCount
// }
