let input = `1326253315
3427728113
5751612542
6543868322
4422526221
2234325647
1773174887
7281321674
6562513118
4824541522`

// input = `5483143223
// 2745854711
// 5264556173
// 6141336146
// 6357385478
// 4167524645
// 2176841721
// 6882881134
// 4846848554
// 5283751526`

// input = `11111
// 19991
// 19191
// 19991
// 11111`

// input = `11
// 19`

let grid = input.split('\n').map(l => l.split('').map(Number))

let flashes = 0;

const checkFlash = (y, x, seen) => {
	if (seen[y+'.'+x]) {
		return
	}
	if (grid[y][x] <= 9) {
		return
	}
	seen[y+'.'+x] = true
	flashes++
	console.log(y, x, 'FLASHED')
	const unfilteredAdjacents = [[y-1, x], [y+1, x], [y, x-1], [y, x+1], [y-1, x-1], [y-1, x+1], [y+1, x+1], [y+1, x-1]]
	const adjacents = unfilteredAdjacents.filter(([y,x]) => {
		// console.log('')
		// console.log('y, x')
		// console.log(y, x)
		// console.log('y >= 0 && y < map.length')
		// console.log(y >= 0 && y < map.length)
		// console.log('y < map.length')
		// console.log(y < map.length)
		return y >= 0 && y < grid.length && x >= 0 && x < grid[y].length
	})
	for (neighboorCoods of adjacents) {
		grid[neighboorCoods[0]][neighboorCoods[1]]++
		checkFlash(neighboorCoods[0], neighboorCoods[1], seen)
	}
}

for (let step = 0; step < 10000; step++) {
	flashes = 0
	let seen = {}
	for (let y = 0; y < grid.length; y++) {
		for (let x = 0; x < grid[y].length; x++) {
			grid[y][x]++
		}
	}
	for (let y = 0; y < grid.length; y++) {
		for (let x = 0; x < grid[y].length; x++) {
			checkFlash(y, x, seen)
		}
	}
	for (let y = 0; y < grid.length; y++) {
		for (let x = 0; x < grid[y].length; x++) {
			if (grid[y][x] > 9) {
				grid[y][x] = 0
			}
		}
	}
	if (flashes == grid.length * grid[0].length) {
		console.log('step', step)
		break
	}
	// console.log(grid)
}

console.log(flashes)